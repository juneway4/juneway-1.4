FROM debian:9
RUN apt update && apt install -y wget make gcc libpcre3 libpcre3-dev zlib1g zlib1g-dev libssl-dev
RUN wget http://nginx.org/download/nginx-1.0.5.tar.gz && tar xvfz nginx-1.0.5.tar.gz && cd nginx-1.0.5 && ./configure && make && make install

FROM debian:9
COPY --from=0 /usr/local/nginx/sbin /usr/local/nginx/sbin
CMD ["/usr/local/nginx/sbin/nginx", "-g", "daemon off"] 
